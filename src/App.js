import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import CarouselInit from './components/CarouselInit';
import Carding from './components/Card';


function App() {
  return (
    <div className="App">
      <Header></Header>
      <br />
      <CarouselInit></CarouselInit>
      <br />
      <div className="container d-flex justify-content-around">
        <Carding></Carding>
        <Carding></Carding>
        <Carding></Carding>
      </div>
    </div>
  );
}

export default App;
