import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

function Header() {
    return (
        <>
            <Navbar bg="primary" data-bs-theme="dark">
                <Container>
                    <Navbar.Brand href="#home">Mon site TP</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="#home">Accueil</Nav.Link>
                        <Nav.Link href="#features">Specificités</Nav.Link>
                        <Nav.Link href="#pricing">Prix</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
        </>
    );
}

export default Header;