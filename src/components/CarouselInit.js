import Carousel from 'react-bootstrap/Carousel';
import Image1 from "../ImageNimp.png"
import Image2 from "../ImageNimp2.jpg"
import Image3 from "../ImageNimp3.jpg"
import StyleColour from "./styleColor.css"

function CarouselInit() {
    return (
        <Carousel style={StyleColour}>
            <Carousel.Item>
                <img src={Image1} />
                <Carousel.Caption>
                    <h5>First slide label</h5>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
            <img src={Image2} />
                <Carousel.Caption>
                    <h5>Second slide label</h5>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
            <img src={Image3} />
                <Carousel.Caption>
                    <h5>Third slide label</h5>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
}

export default CarouselInit;